const express = require("express");
const app = express();
const router = express.Router();

// middleware that is specific to this router
router.use((req, res, next) => {
  console.log("Time: ", Date.now());
  next();
});
// define the home page route
router.get("/", (req, res) => {
  res.send("Birds home page");
});
// define the about route
router.post("/about", (req, res) => {
  res.send("About birds");
});
app.use("/birds", router);

app.listen(process.env.port || 3000);

console.log("Web Server is listening at port " + (process.env.port || 3000));
