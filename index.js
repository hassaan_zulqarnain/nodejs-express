const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const app = express();
const router = express.Router();
const mysql = require("mysql");

// router.get("/home", (req, res) => {
//   res.send("Hello World, This is home router");
// });

// router.get("/profile", (req, res) => {
//   res.send("Hello World, This is profile router");
// });

// router.get("/login", (req, res) => {
//   res.send("Hello World, This is login router");
// });

// router.get("/logout", (req, res) => {
//   res.send("Hello World, This is logout router");
// });

// app.use("/", router);

//---------------------------------------------------------------------//

// function logOriginalUrl(req, res, next) {
//   console.log("Request URL:", req.originalUrl);
//   next();
// }

// function logMethod(req, res, next) {
//   console.log("Request Type:", req.method);
//   next();
// }

// const logStuff = [logOriginalUrl, logMethod];
// app.get("/user/:id", logStuff, (req, res, next) => {
//   res.send("User Info");
// });

//------------------------------------session management---------------------------------//

// app.use(session({ secret: "ssshhhhh", saveUninitialized: true, resave: true }));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));

// var sess; // global session, NOT recommended, only for demonstration purpose

// router.get("/", (req, res) => {
//   sess = req.session;
//   if (sess.email) {
//     return res.redirect("/admin");
//   }
//   res.send("Ok");
// });

// router.post("/login", (req, res) => {
//   sess = req.session;
//   sess.email = req.body.email;
//   res.end("done");
// });

// router.get("/admin", (req, res) => {
//   sess = req.session;
//   if (sess.email) {
//     res.write(`<h1>Hello ${sess.email} h1><br>`);
//     res.end(" " + "-->Logout");
//   } else {
//     res.write("Please login first.");
//     res.end(" " + "-->Login");
//   }
// });

// router.get("/logout", (req, res) => {
//   req.session.destroy((err) => {
//     if (err) {
//       return console.log(err);
//     }
//     res.redirect("/");
//   });
// });

// app.use("/", router);

//-----------------------------------db connection-----------------------------------------------//
const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  pass: "",
  database: "mydb",
});

db.connect((err) => {
  if (err) throw err;
  console.log("connection made");
});
app.get("/createpersontable", (req, res) => {
  let sql =
    "CREATE TABLE persons(id int AUTO_INCREMENT, name varchar(255), age int, PRIMARY KEY(id))";
  db.query(sql, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send("Person Table created");
  });
});

app.get("/addperson", (req, res) => {
  let p1 = {
    name: "Umer",
    age: "22",
  };
  let sql = "INSERT INTO persons SET ?";
  db.query(sql, p1, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send("Person added");
  });
});

app.get("/addperson1", (req, res) => {
  let p1 = {
    name: "Hassaan",
    age: "24",
  };
  let sql = "INSERT INTO persons SET ?";
  db.query(sql, p1, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send("Person1 added");
  });
});

app.get("/getpersons", (req, res) => {
  let sql = "SELECT * FROM persons";
  db.query(sql, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send("all persons fetched");
  });
});

app.get("/getperson/:id", (req, res) => {
  let sql = `SELECT * FROM persons WHERE id = ${req.params.id}`;
  db.query(sql, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send("person fetched");
  });
});

app.get("/updateperson/:id", (req, res) => {
  let newAge = 33;
  let sql = `UPDATE persons SET age = ${newAge} WHERE id = ${req.params.id}`;
  db.query(sql, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send("person updated");
  });
});

app.get("/deleteperson/:id", (req, res) => {
  let sql = `DELETE FROM persons WHERE id = ${req.params.id}`;
  db.query(sql, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send("person deleted");
  });
});
app.listen(process.env.port || 3000);

console.log("Web Server is listening at port " + (process.env.port || 3000));
